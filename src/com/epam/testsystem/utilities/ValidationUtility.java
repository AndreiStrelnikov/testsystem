package com.epam.testsystem.utilities;

import java.util.Date;

public final class ValidationUtility {
    public static boolean isValidTimestamp(final Integer timestamp) {
        return timestamp != null && timestamp > 0;
    }

    public static boolean isValidID(final Long ID) {
        return ID != null && ID > 0;
    }

    public static boolean isEmpty(final Date date) {
        return date == null;
    }

    public static boolean isEmpty(final String string) {
        return string == null || string.isEmpty();
    }
}
