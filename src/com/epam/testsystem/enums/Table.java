package com.epam.testsystem.enums;

public enum Table {
    Achievements,
    Answers,
    Categories,
    Characters,
    Events,
    Games,
    Invites,
    Messages,
    Parameters,
    Players,
    Questions,
    Sessions,
    Statistics,
    Users,
    UsersAchievements,
    Violations,
    WordsForbidden
}
