package com.epam.testsystem.tables;

public interface AbstractTable {
    String getColumnName();
}
