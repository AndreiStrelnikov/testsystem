package com.epam.testsystem.controllers;

import com.epam.testsystem.common.Methods;
import com.epam.testsystem.common.Parameters;
import com.epam.testsystem.common.Paths;
import com.epam.testsystem.entities.ExampleEntity;
import com.epam.testsystem.services.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = Paths.EXAMPLE)
public final class ExampleController extends AbstractController<ExampleEntity> {
    @Autowired
    private ExampleService exampleService;

    @RequestMapping(value = Methods.ECHO, method = RequestMethod.GET)
    public final Result<ExampleEntity> send(@RequestParam(value = Parameters.PARAMETER) final String parameter) {
        return run(() -> exampleService.exampleMethod(parameter));
    }
}
