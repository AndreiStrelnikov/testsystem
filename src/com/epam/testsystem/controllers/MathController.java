package com.epam.testsystem.controllers;

import com.epam.testsystem.common.Methods;
import com.epam.testsystem.common.Parameters;
import com.epam.testsystem.common.Paths;
import com.epam.testsystem.entities.ExampleEntity;
import com.epam.testsystem.entities.MathEntity;
import com.epam.testsystem.services.MathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = Paths.MATH)
public final class MathController extends AbstractController<MathEntity> {
    @Autowired
    private MathService mathService;

    @RequestMapping(value = Methods.SUM, method = RequestMethod.GET)
    public final Result<MathEntity> send(@RequestParam(value = Parameters.ARG1) final Integer arg1,
                                         @RequestParam(value = Parameters.ARG2) final Integer arg2) {
        return run(() -> mathService.add(arg1, arg2));
    }
}
