package com.epam.testsystem.special;

public interface Function<T> {
    public T run() throws Throwable;
}