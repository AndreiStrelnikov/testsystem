package com.epam.testsystem.special;


import com.epam.testsystem.tables.AbstractTable;

import java.util.HashMap;
import java.util.Map;

public final class Arguments {
    private final Map<String, Object> parameters;

    public Arguments() {
        parameters = new HashMap<>();
    }

    public Arguments(final AbstractTable column, final Object value) {
        this();

        add(column, value);
    }

    public final void add(final AbstractTable column, final Object value) {
        parameters.put(column.toString(), value);
    }

    public final Map<String, Object> getParameters() {
        return parameters;
    }
}
