package com.epam.testsystem.services;

import com.epam.testsystem.entities.MathEntity;
import org.springframework.stereotype.Service;

@Service
public final class MathService extends AbstractService<MathEntity> {

    public final MathEntity add(final Integer argument1, final Integer argument2) {
        final MathEntity entity = new MathEntity();
        entity.result = argument1 + argument2;

        return entity;
    }
}
