package com.epam.testsystem.services;

import com.epam.testsystem.entities.ExampleEntity;
import org.springframework.stereotype.Service;

@Service
public final class ExampleService extends AbstractService<ExampleEntity> {

    public final ExampleEntity exampleMethod(final String parameter) {
        final ExampleEntity entity = new ExampleEntity();
        entity.reply = parameter;

        return entity;
    }
}
