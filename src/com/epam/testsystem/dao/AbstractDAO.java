package com.epam.testsystem.dao;

import com.epam.testsystem.entities.AbstractEntity;
import com.epam.testsystem.mappers.AbstractMapper;
import com.epam.testsystem.special.Arguments;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import java.util.HashMap;
import java.util.List;

public abstract class AbstractDAO<E extends AbstractEntity> extends NamedParameterJdbcDaoSupport {
    private final AbstractMapper<E> mapper;

    public AbstractDAO(final AbstractMapper<E> mapper) {
        this.mapper = mapper;
    }

    protected final Long insert(final String query, final Arguments arguments) {
        final KeyHolder keyHolder = new GeneratedKeyHolder();

        if (arguments == null) {
            getNamedParameterJdbcTemplate().update(query, new MapSqlParameterSource(), keyHolder);
            return keyHolder.getKey().longValue();
        }

        getNamedParameterJdbcTemplate().update(query, new MapSqlParameterSource(arguments.getParameters()), keyHolder);
        return keyHolder.getKey().longValue();
    }

    protected final void update(final String query, final Arguments arguments) {
        getJdbcTemplate().update(query, arguments.getParameters());
    }

    protected final List<E> search(final String query, final Arguments arguments) {
        if (arguments == null)
            return getNamedParameterJdbcTemplate().query(query, new HashMap<>(), mapper);

        return getNamedParameterJdbcTemplate().query(query, arguments.getParameters(), mapper);
    }

    protected final E get(final String query, Arguments arguments) {
        if (arguments == null)
            return getNamedParameterJdbcTemplate().queryForObject(query, new HashMap<>(), mapper);

        return getNamedParameterJdbcTemplate().queryForObject(query, arguments.getParameters(), mapper);
    }

    protected final <T> T get(final String query, final Arguments arguments, final Class<T> clazz) {
        if (arguments == null)
            return getNamedParameterJdbcTemplate().queryForObject(query, new HashMap<>(), clazz);

        return getNamedParameterJdbcTemplate().queryForObject(query, arguments.getParameters(), clazz);
    }

    protected final <T> T get(final String query, final Arguments arguments, final Class<T> clazz, final T ifNull) {
        try {
            return get(query, arguments, clazz);
        } catch (final Exception ignored) {}

        return ifNull;
    }

    protected final Boolean isEmpty(final String query, final Arguments arguments) {
        return search(query, arguments).isEmpty();
    }
}
