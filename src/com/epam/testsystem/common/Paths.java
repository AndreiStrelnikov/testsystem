package com.epam.testsystem.common;

public final class Paths {
    private final static String SERVICES = "/services";

    public final static String EXAMPLE = SERVICES + Services.EXAMPLE;
    public final static String MATH    = SERVICES + Services.MATH;
}
